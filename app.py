
from flask import Flask, jsonify, request
import os,io
from google.cloud import vision
from numpy  import array, squeeze, ones, uint8,float16
from cv2 import imread,imwrite,cornerHarris, threshold,bitwise_not,erode,dilate,getStructuringElement,minAreaRect,boxPoints,addWeighted,rectangle,boundingRect,contourArea,moments,moments,findContours,RETR_TREE,CHAIN_APPROX_NONE,THRESH_BINARY,THRESH_OTSU,MORPH_DILATE
from shapely.geometry.polygon import Polygon
from PIL import Image, ImageDraw, ImageFont
import unicodedata
from collections import OrderedDict 
app = Flask(__name__)

UPLOAD_FOLDER = os.path.basename('.')
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = r"GoogleVIsion.json"
client = vision.ImageAnnotatorClient()

def ocr_text(file):
    """faz a detecção de caracteres em uma imagem (OCR) utilizando a API do Google Vision"""
    try:
        f = os.path.join(app.config['UPLOAD_FOLDER'], file.filename)
        file.save(f)
        
        with io.open(UPLOAD_FOLDER+"/"+file.filename, 'rb') as image_file:
            content = image_file.read()

        image = vision.types.Image(content=content)
        response = client.text_detection(image=image) 
    except Exception as e:
        response = "Unreadable image!"
    return response

def find_polygons(response):
    """Retorna o retângulo que envolve cada palavra encontrada pela API do Google Vision"""
    polygons = [] # Polígonos que envolvem as palavras (oriundos da API do Google Vision)
    for i in range(len(response.text_annotations[1:])):
        points = response.text_annotations[i+1].bounding_poly.vertices
        description = response.text_annotations[i+1].description
        polygon = Polygon([[points[0].x, points[0].y],
                           [points[1].x, points[1].y],
                           [points[2].x, points[2].y],
                           [points[3].x, points[3].y]])
        polygons.append((polygon,description))
    return polygons

def plot_polygons_image(file_path,polygons):
"""Faz a plotagem dos retângulos em volta de cada palavra lida"""
    im = Image.open(file_path)
    draw = ImageDraw.Draw(im)
    for polygon, _ in polygons:
        draw.polygon(polygon.exterior.coords[:4], None, 'red')
    im = array(im)
    return im

def preprocess_image(file_path,alpha = 0.5):
    """Remove ruídos, tranforma a imagem em escala de cinza e devolve apenas a estrutura encontrada"""
    try:
        beta = 1.0 - alpha
        img = imread(file_path, 0)
        _, thresh = threshold(img, 128, 255,THRESH_BINARY|THRESH_OTSU)
        bitwise = bitwise_not(thresh)
        erosion = erode(bitwise, ones((1, 1) ,uint8), iterations=4)
        img_bin = dilate(erosion, ones((1, 1) ,uint8), iterations=4)
        kernel_length = array(img_bin).shape[1]//120
        verticle_kernel = getStructuringElement(MORPH_DILATE, (1, kernel_length))
        hori_kernel = getStructuringElement(MORPH_DILATE, (kernel_length, 1))
        kernel = getStructuringElement(MORPH_DILATE, (3, 3))
        img_temp1 = erode(img_bin, verticle_kernel, iterations=4)
        verticle_lines_img = dilate(img_temp1, verticle_kernel, iterations=4)
        img_temp2 = erode(img_bin, hori_kernel, iterations=4)
        horizontal_lines_img = dilate(img_temp2, hori_kernel, iterations=4)
        img_final_bin = addWeighted(verticle_lines_img, alpha, horizontal_lines_img, beta, 0.0)
        img_final_bin = erode(~img_final_bin, kernel, iterations=4)
        _, img_final_bin = threshold(img_final_bin, 128,255, THRESH_BINARY | THRESH_BINARY)
    except Exception as e:
        return "error processing image"
    return img_final_bin

def complete_contours(image):
    """Completa os campos que são formados a partir do processamento de imagem"""
    dst = cornerHarris(image,4,3,0.07)
    thresh = float16(0.01*dst.max())
    for y in range(dst.shape[0]):
        for x in range(dst.shape[1]):
            if (dst[y,x] > thresh):
                rectangle(image, (x,y-30), (x+10,y+30), (0,0,255), 2)
                rectangle(image, (x-200,y), (x+200,y+1), (0,0,255), 2)
    return image

def find_in_box(box,polygons):
    box= Polygon(squeeze(box))
    texto = ''
    for polygon, desc in polygons:
        if box.contains(polygon.centroid):
            texto += f" {desc}"
    return texto.strip()
    
def sort_contours(cnts, method="left-to-right"):
     
    reverse = False
    i = 0

    if method == "right-to-left" or method == "bottom-to-top":
        reverse = True

    if method == "top-to-bottom" or method == "bottom-to-top":
        i = 1

    boundingBoxes = [boundingRect(c) for c in cnts]
    (cnts, boundingBoxes) = zip(*sorted(zip(cnts, boundingBoxes),
        key=lambda b:b[1][i], reverse=reverse))

    return cnts, boundingBoxes


def filter_contours(contours,poligonos, image):
    img_width,img_height = image.shape[1],image.shape[0]
    valid_contours,textos_caixa = [], []
    contours = list(filter(lambda c: 
                       contourArea(c) > img_width* img_height*0.00057 and
                       contourArea(c) < img_width* img_height*0.085,
                       contours))           
    for contour in contours:
        texto = find_in_box(contour,poligonos)
        if texto != '':
            valid_contours.append(contour)
    valid_contours,_ = sort_contours(valid_contours)

    for contour in valid_contours:
        rect = minAreaRect(contour)
        four_coords = boxPoints(rect)
        textos_caixa.append((find_in_box(contour,poligonos),four_coords))
    return valid_contours, textos_caixa
    
def remove_accents(input_str):
    """Padroniza o texto (remove acentos e deixa em caixa alta"""
    nfkd_form = unicodedata.normalize('NFKD', input_str.upper().replace("\n"," "))
    return u"".join([c for c in nfkd_form if not unicodedata.combining(c)])

@app.route('/api/box_extraction', methods=['POST','GET'])
def upload_file():
    if request.method == "GET":
        return jsonify({"TEXTO": "Seja Bem vindo a API de OCR, faça uma requisição POST e envie os parâmetros necessários para sua consulta"})
    elif request.method == "POST":
        
        arquivo = request.files['image']
        response = ocr_text(arquivo)
        if response.ByteSize() == 0 or response == "Unreadable image!" : 
            os.remove(UPLOAD_FOLDER+"/"+arquivo.filename)
            return  jsonify({"response":"Nothing to read"})
            
        poligonos = find_polygons(response)
        im = plot_polygons_image(arquivo,poligonos)
        img_final = preprocess_image(UPLOAD_FOLDER+"/"+arquivo.filename)
        try:
            img_final = complete_contours(img_final)
            contours,_ = findContours(img_final, RETR_TREE, CHAIN_APPROX_NONE)
            contours,textos_caixa = filter_contours(contours,poligonos,im)
        except Exception as e:
            os.remove(UPLOAD_FOLDER+"/"+arquivo.filename)
            return  jsonify({"response": "error processing image"})
        
        os.remove(UPLOAD_FOLDER+"/"+arquivo.filename)
        
        resposta = OrderedDict({"complete_text":response.text_annotations[0].description,
                                "blocks": []})
        for num ,texto_caixa in enumerate(textos_caixa):
            texto, caixa = texto_caixa
            resposta["blocks"].append(OrderedDict({"id":num+1,
                                       "text": remove_accents(texto),
                                       "coordinates":caixa.tolist()}))       
    return  jsonify(resposta)
    
if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')