FROM python:3.8
COPY . /root
ADD ./credentials/GoogleVIsion.json /root
WORKDIR /root
RUN pip install -r requirements.txt
ENTRYPOINT ["python"]
CMD ["app.py"]